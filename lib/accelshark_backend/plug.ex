defmodule AccelsharkBackend.Plug do
  use Plug.Builder

  plug Plug.Logger
  plug :redirect_root
  plug Plug.Static, at: "/", from: {:accelshark_backend, "priv/static"}, brotli: true
  plug :not_found

  @spec redirect_root(Plug.Conn.t(), any) :: Plug.Conn.t()
  def redirect_root(conn, _) do
    case conn.path_info do
      [] ->
        %{conn | path_info: ["index.html"]}

      _ ->
        conn
    end
  end

  @spec not_found(Plug.Conn.t(), any) :: Plug.Conn.t()
  def not_found(conn, _) do
    conn |> send_resp(404, "404 Not Found")
  end
end
