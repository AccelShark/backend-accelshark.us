FROM elixir:1.10.4-alpine

ADD . /opt/accelshark
WORKDIR /opt/accelshark

ARG MIX_ENV=prod
RUN mix local.hex --force && mix local.rebar --force && mix release

ENTRYPOINT [ "/opt/accelshark/_build/prod/rel/accelshark_backend/bin/accelshark_backend", "start_iex" ]